class HomeController < ApplicationController
  def index
    flash[:notice] = "Notice text assigned in HomeController#index ('/' page)"
  end
end
