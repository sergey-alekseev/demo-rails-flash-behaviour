Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get 'step-1', to: 'home#step1'
  get 'step-2', to: 'home#step2'
  get 'step-3', to: 'home#step3'
end
